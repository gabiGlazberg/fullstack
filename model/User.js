var mongoose = require('mongoose');
const plm = require('passport-local-mongoose');
var userSchema = mongoose.Schema({
    username: String,
    password: String,
    role: {type:Number, default: 0}
});
userSchema.plugin(plm);
var User = mongoose.model('users', userSchema);

module.exports = User;
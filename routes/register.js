var express = require('express');
var router = express.Router();
const passport = require('passport');
const User = require('../model/User');
/* GET users listing. */
router.get('/', function (req, res, next) {
	res.render('register', {
	    title: 'Register'
	});
});

router.post('/' , function (req, res, next) {
	User.register(new User({
			username: req.body.username,
			role: req.body.role
	}),
		req.body.password,
		function (err, user) {
			if (err) {
				console.log(req.body.password)
				throw err;
				res.redirect('register');
			} else {
				console.log(req.body.role)
				passport.authenticate('local')(req, res, function () {
					res.redirect('/')
				});
			}
		}
	);
	// res.render('register', {
	//     title: 'Register'
	// });
});

module.exports = router;
var express = require('express');
var router = express.Router();
const passport = require('passport')
const User = require('../model/User');
var mongoose = require('mongoose');


/* GET users listing. */
router.get('/', function(req, res, next) {
    User.find({} , function(err, row){
        if(err) throw err;
        console.log(row);
        res.send(row);
    });
});


router.post('/', function(req, res, next) {
    User.register(new User({
        username: req.body.username,
        role: req.body.role
}),
    req.body.password,
    function (err, user) {
        if (err) {
            console.log(req.body.password)
            throw err;
            res.redirect('register');
        } else {
            console.log(req.body.role)
            passport.authenticate('local')(req, res, function () {
                res.redirect('/');
            });
        }
    }
);  
  res.redirect('/');
});

module.exports = router;
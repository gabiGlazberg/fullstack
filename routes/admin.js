var express = require('express');
var router = express.Router();
var adminAuth = require('../meddle_ware/admin');

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('admin route');
  res.send('/index/login');
});

module.exports = router;

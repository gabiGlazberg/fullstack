var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    console.log('out')
    req.logout();
    res.render('/login');
});

module.exports = router;
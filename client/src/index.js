import angular from 'angular';

import {hello} from './app/hello';
import {admin} from './app/admin';
import {login} from './app/login';

import 'angular-ui-router';
import routesConfig from './routes';

import './index.scss';

export const app = 'app';

angular
  .module(app, ['ui.router'])
  .config(routesConfig)
  .component('app', hello)
  .component('admin', admin)
  .component('login', login);

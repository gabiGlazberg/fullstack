export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'app'
    })
    // .state('admin', {
    //   url: '/admin',
    //   component: 'admin'
    // })
    // .state('login', {
    //   url: '/login',
    //   component: 'login'
    // })
    .state('logout', {
      url: '/logout',
      component: 'login'
    });
}

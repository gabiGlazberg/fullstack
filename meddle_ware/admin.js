const passport = require('passport');

const auth = (req, res, next) => {
    if (req.user) {
        if (req.user.role === '1') {
            next();
        } else {
            res.redirect('/');
        }
    } else {
        res.redirect('/');
    }
}
module.exports = auth;